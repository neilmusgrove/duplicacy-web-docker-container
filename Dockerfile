FROM alpine:latest

ENV DUPLICACY_WEB_VERSION=1.3.0

# Set to actual USR_ID and GRP_ID of the user this should run under
# Uses root by default, unless changed

ENV USR_ID=0 \
    GRP_ID=0 

ENV TZ="Europe/London"

# Installing software
RUN apk --update add --no-cache bash ca-certificates dbus curl su-exec tzdata           && \
    curl -s -o /usr/local/bin/duplicacy_web                                           \
        https://acrosync.com/duplicacy-web/duplicacy_web_linux_arm_${DUPLICACY_WEB_VERSION} 2>&1 && \
    curl -s -o /usr/local/bin/launch.sh                                           \
        https://bitbucket.org/neilmusgrove/duplicacy-web-docker-container/raw/63f783d9d7c3288b411c0c4cf132bb48d9845387/launch.sh 2>&1 && \
    curl -s -o /usr/local/bin/init.sh                                           \
        https://bitbucket.org/neilmusgrove/duplicacy-web-docker-container/raw/63f783d9d7c3288b411c0c4cf132bb48d9845387/init.sh 2>&1 && \
    chmod +x /usr/local/bin/duplicacy_web                                           && \
    chmod +x /usr/local/bin/launch.sh                                           && \
    chmod +x /usr/local/bin/init.sh                                          && \
    rm -f /var/lib/dbus/machine-id && ln -s /config/machine-id /var/lib/dbus/machine-id 

EXPOSE 3875/tcp
VOLUME /config /logs /cache

ENTRYPOINT /usr/local/bin/init.sh
